package com.rest.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java8CaseStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java8CaseStudyApplication.class, args);
    }
}
