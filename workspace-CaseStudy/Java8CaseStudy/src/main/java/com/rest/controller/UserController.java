package com.rest.controller;

import java.io.UnsupportedEncodingException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.rest.model.User;

@RestController("userController")
@RequestMapping("/users")
public class UserController {
	
	@RequestMapping(method = RequestMethod.GET, value = "/{Name}")
	public ResponseEntity<User> getAllRecord(@PathVariable("Name") String Name) {
		User user = new User();
        user.setName(Name);
        user.setEmail("Shivakumara@gmail.com");
        user.setAge(20);
        return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	/*ResponseEntity<Message> get() {
	    Message message = new Message(penguinCounter.incrementAndGet() + " penguin!");
	    return new ResponseEntity<Message>(message, HttpStatus.OK);
	}*/

	
	
	@RequestMapping("/something")
	public ResponseEntity<String> handle(HttpEntity<byte[]> requestEntity) throws UnsupportedEncodingException {
	    String requestHeader = requestEntity.getHeaders().getFirst("MyRequestHeader");
	    byte[] requestBody = requestEntity.getBody();

	    // do something with request header and body

	    HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("MyResponseHeader", "MyValue");
	    return new ResponseEntity<String>("Hello World", responseHeaders, HttpStatus.CREATED);
	}
}
