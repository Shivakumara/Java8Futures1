package com.rest.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewJava8CaseStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewJava8CaseStudyApplication.class, args);
    }
}
