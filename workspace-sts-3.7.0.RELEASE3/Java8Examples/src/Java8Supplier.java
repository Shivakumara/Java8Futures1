import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Java8Supplier {
	
	/*public static void main(String[] args) {
	
        Supplier<Item> supplier = Item::new;
        Item item = supplier.get();
        System.out.println(item.getMsg());
    }*/

	
	public static void main(String[] args) {
        List<Item> list = new ArrayList<>();
        list.add(new Item("AA"));
        list.add(new Item("BB"));
        list.add(new Item("CC"));
        Stream<String> names = list.stream().map(Item::getName);
        names.forEach(n -> System.out.println(n));
    }  
	
	
	/*public static void main(String[] args) {
        Supplier<String> supplier = Item::getStaticVal;
        String val = supplier.get();
        System.out.println("Calling  Method:"+val);
    }  */  
	
}



class Item {
    private String name;
    public Item(){     
    }
    public Item(String name){     
        this.name=name;
    }
    public static String getStaticVal(){
        return "Static Val";
    }
    public String getMsg(){
        return "Hello World!";
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
} 

