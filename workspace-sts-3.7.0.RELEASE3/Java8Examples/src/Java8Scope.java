
public class Java8Scope {
	final static String salutation = "Hello! ";

	public static void main(String args[]) {
		GreetingService greetService1 = message -> System.out.println(salutation + message);
		greetService1.sayMessage("Shiva ");
	}

	interface GreetingService {
		void sayMessage(String message);
	}
}