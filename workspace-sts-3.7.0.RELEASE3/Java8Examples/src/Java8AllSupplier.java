import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;

public class Java8AllSupplier {
	
	
	/*public static void main(String[] args) {
        LItem item = new LItem(true,123);
        BooleanSupplier bs = item::isStatus; 
        System.out.println("Status:"+bs.getAsBoolean());
    }*/

	
	
	/*public static void main(String[] args) {
        LItem item = new LItem(true,123);
        IntSupplier is = item::getVal; 
        System.out.println("Int Value:"+is.getAsInt());
    }*/
	
	
	
	/*public static void main(String[] args) {
        LItem item = new LItem(true,123);
        LongSupplier ls = item::getVal; 
        System.out.println("Long Value:"+ls.getAsLong());        
    } */
	
	
	public static void main(String[] args) {
        LItem item = new LItem(true,123);
        DoubleSupplier ds = item::getVal; 
        System.out.println("Double Value:"+ds.getAsDouble());        
    }  
	
}



class LItem {
    private Boolean status;
    private Integer val;
    public LItem(Boolean status, Integer val){
        this.status=status;
        this.val=val;
    }
    public Boolean isStatus() {
        return status;
    }
    public Integer getVal() {
        return val;
    }
} 