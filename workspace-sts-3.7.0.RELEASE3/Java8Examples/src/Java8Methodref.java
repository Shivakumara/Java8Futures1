
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.*;

public class Java8Methodref {
	public static void main(String args[]) {
		List names = new ArrayList();
		names.add("Mahesh");
		names.add("Suresh");
		names.add("Ramesh");
		names.add("Naresh");
		names.add("Kalpesh");
		names.forEach(System.out::println);

		Map<Integer, String> cm = new HashMap<Integer, String>();
		cm.put(1, "AAA");
		cm.put(2, "BBB");
		cm.put(3, "CCC");
		BiConsumer<Integer, String> biConsumer = (k, v) -> System.out.println("Key:" + k + " Value:" + v);
		cm.forEach(biConsumer);

	}
}