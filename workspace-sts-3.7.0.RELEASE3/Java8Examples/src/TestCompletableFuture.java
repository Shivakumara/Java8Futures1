import java.util.concurrent.CompletableFuture;

public class TestCompletableFuture {

	

	public static void main(String args[]) {
		
		CompletableFuture completableFutureToBeCompleted2 = CompletableFuture.supplyAsync(() -> {
			int i=0;
			for (i = 0; i < 10; i--) {
				//System.out.println("i " + i);
			}
			return i;
		});

		CompletableFuture completor = CompletableFuture.supplyAsync(() -> {
			System.out.println("completing the other");
			completableFutureToBeCompleted2.complete(43);
			return 10;
		});
		
		try{
		System.out.println( "Second Future Value - > " +completor.get() );
		System.out.println( "First Future Value - > " +completableFutureToBeCompleted2.get() +"  Status  "+completableFutureToBeCompleted2.isDone());
		}catch(Exception ex)
		{
			System.out.println("Exception in get Completable Future "+ex);
		}
		
	}

}
