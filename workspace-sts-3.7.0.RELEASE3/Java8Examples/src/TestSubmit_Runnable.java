import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestSubmit_Runnable {

	public static void main(String args[]) {
		try {
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			Future future = executorService.submit(new Runnable() {
				public void run() {
				System.out.println("Asynchronous task");
				}
				});
				System.out.println(future.get());
		} catch (Exception ex) {

		}
	}

	
}
