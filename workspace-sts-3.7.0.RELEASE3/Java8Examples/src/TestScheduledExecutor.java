import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TestScheduledExecutor {

	/*
	 * schedule (Callable task, long delay, TimeUnit timeunit) schedule
	 * (Runnable task, long delay, TimeUnit timeunit) scheduleAtFixedRate
	 * (Runnable, long initialDelay, long period, TimeUnit timeunit)
	 * scheduleWithFixedDelay (Runnable, long initialDelay, long period,
	 * TimeUnit timeunit)
	 */

	public static void main(String args[]) {
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
		ScheduledFuture scheduledFuture = scheduledExecutorService.schedule(new Callable() {
			public Object call() throws Exception {
				System.out.println("Executed!");
				return "Called!";
			}
		}, 5, TimeUnit.SECONDS);

		
		
		scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			public void run() {
				System.out.println(" scheduleAtFixedRate Executed!");
				// return "Called!";
			}
		}, 1, 1, TimeUnit.SECONDS);
	}

}
