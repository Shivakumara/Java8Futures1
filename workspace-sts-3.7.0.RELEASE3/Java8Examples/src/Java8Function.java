
import java.util.function.DoubleFunction;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleToLongFunction;
import java.util.function.Function;

//from  w w  w  .  ja va 2s  .  c  o m
public class Java8Function {

	/*public static void main(String[] args) {
		Function<Integer, String> converter = (i) -> Integer.toString(i);

		System.out.println(converter.apply(3).length());
		System.out.println(converter.apply(30).length());
	}*/
	
	
	
	
	
	/*public static void main(String[] args)  {

	    String result = calc((a) -> "Result: " + (a * 2), 10);

	    System.out.println(result);

	  }
	  public static String calc(Function<Integer, String> bi, Integer i) {
	    return bi.apply(i);
	  }  
	  */
	  
	
	
	/*public static void main(String[] args) {
        DoubleFunction<String> df = d -> String.valueOf(d*5.3); 
        System.out.println(df.apply(43.7));
    }  */  
	
	
	  
	 /* public static void main(String[] args) {
	        DoubleToIntFunction ob = f -> new Double(f*4.8).intValue();
	        System.out.println(ob.applyAsInt(43.7));
	    } */ 
	
	
	public static void main(String[] args) {
        DoubleToLongFunction ob = f -> new Double(f*4.8).longValue();
        System.out.println(ob.applyAsLong(43.7));
    }  
	  
	  
}