import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestSubmit_Callable {

	public static void main(String args[]) {
		try {
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			Future future = executorService.submit(new Callable() {
				public Object call() throws Exception {
					System.out.println("Asynchronous Callable");
					return "Callable Result";
				}
			});
			executorService.shutdown();
			System.out.println("future.get() = " + future.get());
		} catch (Exception ex) {

		}
	}

}
